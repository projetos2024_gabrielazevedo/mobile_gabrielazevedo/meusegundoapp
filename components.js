import { StyleSheet, Text, View, ScrollView, TextInput, Button } from 'react-native';
import { useState } from 'react';

export default function Components() {
        const [text, setText] = useState("");
        function click(){

        }
  return (
<View style={styles.container}>
    <Text style={styles.text}> Seja Bem Vindo, JAPA 16! {text} </Text>
    <TextInput style={styles.input} placeholder="Digite algo..." value={text} onChangeText={(TextInput)=> setText (TextInput)}/>
    <Button title="Clique" onPress={()=>{}}/>
</View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },

  text: {
    fontSize:24,
    fontWeight:"bold",
  },

  input:{
    borderWidth:1,
    borderColor:"gray",
    width:"80%",
    padding:10,
    marginVertical:10,
  }

});