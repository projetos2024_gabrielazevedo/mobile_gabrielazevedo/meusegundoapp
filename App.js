import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import Layout from './layoutDeTelaEstrutura';
import LayoutHorizontal from './layoutHorizontal'
import LayoutGrade from './layoutGrade';
import Components from './components';

export default function App() {
  return (

<View style={styles.container}>


<Layout></Layout>
<View style={styles.seta}>
        <Text style={styles.setaIcon}>➤</Text>
      </View>


</View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',

  },
  seta: {
    zIndex: 1000,
    position: 'absolute',
    left: 2,
    top: 350,
    height: 50,
    width: 50,
    alignItems: 'center'
  },
  setaIcon: {
    fontSize: 40,
    color: 'red',
  },
});

