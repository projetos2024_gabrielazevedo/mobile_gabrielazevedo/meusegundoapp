import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import Layout from './layoutDeTelaEstrutura';

export default function LayoutHorizontal() {
  return (
<View style={styles.container}>
<ScrollView horizontal={true}>
    <View style={styles.box1}></View>
    <View style={styles.box2}></View>
    <View style={styles.box3}></View>
    <View style={styles.box1}></View>
    <View style={styles.box2}></View>
    <View style={styles.box3}></View>
    <View style={styles.box1}></View>
    <View style={styles.box2}></View>
    <View style={styles.box3}></View>
    <View style={styles.box1}></View>
    <View style={styles.box2}></View>
    <View style={styles.box3}></View>
</ScrollView>

</View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor:"black"

  },

  box1: {
    width:60,
    height:60,
    backgroundColor:"#7890a8",
    borderRadius:60,
  },

  box2: {
    width:80,
    height:80,
    backgroundColor:"#304878",
    borderRadius:80,
  },

  box3: {
    width:100,
    height:100,
    backgroundColor:"#181848",
    borderRadius:100,
  },

});
